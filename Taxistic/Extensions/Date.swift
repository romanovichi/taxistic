//
//  Date.swift
//  Taxistic
//
//  Created by Иван Романович on 22.09.2018.
//  Copyright © 2018 Иван Романович. All rights reserved.
//

import Foundation

extension Date {
    
    var day: Int {
        return Calendar.current.component(.day, from: self)
    }
    
    var month: Int {
        return Calendar.current.component(.month, from: self)
    }
    
    var week: Int {
        return Calendar.current.component(.weekOfYear, from: self)
    }
    
    var year: Int {
        return Calendar.current.component(.year, from: self)
    }
    
    var shortTimeStyle: String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .none
        dateFormatter.timeStyle = .short
        return dateFormatter.string(from: self)
    }
    
    var startOfWeek: Date? {
        let gregorian = Calendar(identifier: .gregorian)
        guard let sunday = gregorian.date(from: gregorian.dateComponents([.yearForWeekOfYear, .weekOfYear], from: self)) else { return nil }
        return gregorian.date(byAdding: .day, value: 1, to: sunday)
    }
    
    var endOfWeek: Date? {
        let gregorian = Calendar(identifier: .gregorian)
        guard let sunday = gregorian.date(from: gregorian.dateComponents([.yearForWeekOfYear, .weekOfYear], from: self)) else { return nil }
        return gregorian.date(byAdding: .day, value: 7, to: sunday)
    }
    
    var isDateinThisMounth: Bool {
        
        let calendar = Calendar.current
        let currentDate = Date()
        
        let componentsToGetStartOfMonth = calendar.dateComponents([.year, .month], from: currentDate)
        let startOfMonth = calendar.date(from: componentsToGetStartOfMonth)
        
        var componentsToGetEndOfMonth = DateComponents()
        componentsToGetEndOfMonth.month = 1
        componentsToGetEndOfMonth.day = -1
        let endOfMonth = calendar.date(byAdding: componentsToGetEndOfMonth, to: startOfMonth!)
        
        let range = startOfMonth!...endOfMonth!
        
        if range.contains(self) {
            return true
        } else {
            return false
        }
    }
}
