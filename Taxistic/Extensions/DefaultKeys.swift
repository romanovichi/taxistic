//
//  DefaultKeys.swift
//  Taxistic
//
//  Created by Иван Романович on 10.01.2019.
//  Copyright © 2019 Иван Романович. All rights reserved.
//

import Foundation
import SwiftyUserDefaults

extension DefaultsKeys {
    static let fuel = DefaultsKey<Double>("fuel")
    static let goal = DefaultsKey<Double>("goal")
    static let subsidyIsOn = DefaultsKey<Bool>("subsidyIsOn")
}
