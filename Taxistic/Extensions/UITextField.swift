//
//  UITextField.swift
//  Taxistic
//
//  Created by Иван Романович on 19.09.2018.
//  Copyright © 2018 Иван Романович. All rights reserved.
//

import UIKit

@IBDesignable extension UITextField {
    
    @IBInspectable var keyboard:Int{
        get{
            return self.keyboardType.rawValue
        }
        set(keyboardIndex){
            self.keyboardType = UIKeyboardType.init(rawValue: keyboardIndex)!
            
        }
    }
    
}
