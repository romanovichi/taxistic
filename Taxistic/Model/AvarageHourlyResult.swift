//
//  AvarageHourlyResult.swift
//  Taxistic
//
//  Created by Иван Романович on 14.11.2018.
//  Copyright © 2018 Иван Романович. All rights reserved.
//

import Foundation

class AvarageHourlyResult {
    
    var ordersAvarage: Int = 0
    var profitAvarage: Double = 0
}
