//
//  MainData.swift
//  Taxistic
//
//  Created by Иван Романович on 14.09.2018.
//  Copyright © 2018 Иван Романович. All rights reserved.
//

import Foundation
import RealmSwift

class MainData: Object {
    
    @objc dynamic var id = 0
    @objc private dynamic var _startTime: NSDate? = nil
    
    var startTime: Date? {
        set {
            _startTime = newValue as NSDate?
        }
        get {
            if let startTime = _startTime {
                return startTime as Date
            }
            return Date()
        }
    }
    
    convenience init(withStartTime startTime: Date) {
        self.init()
        self.startTime = startTime
    }
    
    var orders = List<Order>()
    
    override class func primaryKey() -> String? {
        return "id"
    }
}
