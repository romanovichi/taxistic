//
//  Order.swift
//  Taxistic
//
//  Created by Иван Романович on 29.09.2018.
//  Copyright © 2018 Иван Романович. All rights reserved.
//

import Foundation
import RealmSwift

class Order: Object {
    
    @objc dynamic var id = UUID().uuidString
    @objc dynamic var cost: Double = 0
    @objc dynamic var profit: Double = 0
    @objc dynamic var subsidyDescription: String = ""
    @objc dynamic var distance: Double = 0
    @objc dynamic var fuelСonsumption: Double = 0
    @objc dynamic private var _endTime: NSDate? = nil
    
    var endTime: Date {
        set {
            _endTime = newValue as NSDate?
        }
        get {
            if let endTime = _endTime {
                return endTime as Date
            }
            return Date()
        }
    }
    
    convenience init(cost: Double, profit: Double, subsidyDescription: String, distance: Double, fuelСonsumption: Double, endTime: Date) {
        self.init()
        self.cost = cost
        self.profit = profit
        self.subsidyDescription = subsidyDescription
        self.distance = distance
        self.fuelСonsumption = fuelСonsumption
        self.endTime = endTime
    }
    
    override class func primaryKey() -> String? {
        return "id"
    }
}
