//
//  ResultForDay.swift
//  Taxistic
//
//  Created by Иван Романович on 14.11.2018.
//  Copyright © 2018 Иван Романович. All rights reserved.
//

import Foundation

class ResultForPeriod {
    
    var date = Date()
    var ordersTotal: Double = 0
    var profitTotal: Double = 0
    var distanceTotal: Double = 0
    var hoursSpent: Double = 0
    var fuelTotal: Double = 0
    var avarageProfit: Double = 0
    
}
