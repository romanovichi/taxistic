//
//  Protocols.swift
//  Taxistic
//
//  Created by Иван Романович on 14.09.2018.
//  Copyright © 2018 Иван Романович. All rights reserved.
//

import Foundation
import RealmSwift

protocol TaxiPark {
    func getNetProfit(for cost: Double) -> (Double, String)
}
