//
//  Repository.swift
//  Taxistic
//
//  Created by Иван Романович on 14.09.2018.
//  Copyright © 2018 Иван Романович. All rights reserved.
//

import Foundation
import RealmSwift

class Repository<T: Object> {
    
    let realm = try! Realm()
    
    func updateById(_ object: T) {
        try? realm.write {
            realm.add(object, update: true)
        }
    }
    
    func update(completion: @escaping ()->()) {
        try? realm.write {
            completion()
        }
    }
    
    func cleanMainData(_ object: T) {
        try? realm.write {
            object.setValue(0, forKey: "totalOrders")
            object.setValue(0, forKey: "totalProfit")
            object.setValue(0, forKey: "totalFuelСonsumption")
        }
    }

    func get() -> [T] {
        return realm.objects(T.self).map { $0 }
    }
    
    func delete() {
        try? realm.write {
            realm.deleteAll()
        }
    }
}

