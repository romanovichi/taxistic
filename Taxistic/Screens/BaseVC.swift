//
//  BaseVC.swift
//  Taxistic
//
//  Created by Иван Романович on 19.09.2018.
//  Copyright © 2018 Иван Романович. All rights reserved.
//

import UIKit

class BaseVC: UIViewController {
    
    @IBOutlet weak var navigationBar: CustomNavigationView!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
    }
}
