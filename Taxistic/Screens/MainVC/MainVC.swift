//
//  MainVC.swift
//  Taxistic
//
//  Created by Иван Романович on 13.09.2018.
//  Copyright © 2018 Иван Романович. All rights reserved.
//

import UIKit
import SwiftyUserDefaults

class MainVC: BaseVC {
    
    @IBOutlet weak var totalProfitLabel: UILabel!
    @IBOutlet weak var totalOrdersLabel: UILabel!
    @IBOutlet weak var totalFuelLabel: UILabel!
    @IBOutlet weak var costLabel: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var startTimeLabel: UILabel!
    @IBOutlet weak var ordersLeftToDailyGoalLabel: UILabel!
    @IBOutlet weak var dailyGoalLabel: UILabel!
    
    @IBOutlet weak var taxiParkSegmentedControl: UISegmentedControl!
    @IBOutlet weak var costSliderControl: UISlider!
    @IBOutlet weak var distanceSliderControl: UISlider!
    @IBOutlet weak var profitProgressView: UIProgressView!
    
    @IBOutlet weak var informationContainerView: UIView!
    @IBOutlet weak var costContainerView: UIView!
    @IBOutlet weak var distanceContainerView: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var pullUpViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var addButton: UIButton!
    
    private var adapter: TableViewAdapter!
    private var dataService = OrdersDataService()
    private var sortingService = OrdersSortingService()
    private var calculaionService = OrdersCalculationService()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupGestureRecogizers()
        
        navigationBar.addNavigationButton(.settingsButton) {
            
            let settings = SettingsVC()
            self.navigationController?.pushViewController(settings, animated: true)
            
            settings.onCancel = { [weak self] in
                self?.dataService.removeLastOrder()
                self?.updateFields()
            }
        }
        
        navigationBar.addNavigationButton(.statisticsButton) {
            let orders = self.dataService.getAllOrders()
            let statistics = StatisticsVC(orders: orders)
            self.navigationController?.pushViewController(statistics, animated: true)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.updateFields()
    }
}

extension MainVC {
    
    // MARK: Fields setup
    
    private func updateFields() {
        
        let mainData = dataService.get()
        var ordersForToday: [Order] = []
        
        if let time = mainData.startTime {
            startTimeLabel.text = time.shortTimeStyle
            ordersForToday = sortingService.getOrdersFor(date: time, from: dataService.getAllOrders())
        }
        
        adapter = TableViewAdapter(tableView: tableView, orders: ordersForToday)
        
        totalOrdersLabel.text = String(Int(ordersForToday.count))
        totalProfitLabel.text = String(Int(calculaionService.getTotalProfit(for: ordersForToday)))
        totalFuelLabel.text = String(Int(calculaionService.getTotalFuelСonsumption(for: ordersForToday)))
        dailyGoalLabel.text = String(Int(Defaults[.goal]))
        ordersLeftToDailyGoalLabel.text = String(Int(calculaionService.getOrdersAmountToReachDailyGoal(ordersForToday, calculaionService.getAvagageProfit(for: dataService.getAllOrders()))))
        profitProgressView.setProgress(calculaionService.getProgress(for: ordersForToday), animated: true)
    }
    
    private func animatePullUpView(withText text: String, andHeightConstraintValue value: CGFloat) {
        addButton.setTitle(text, for: .normal)
        pullUpViewHeightConstraint.constant = value
        
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
    }
}

private extension MainVC {
    
    @IBAction func calculateButtonPressed(_ sender: Any) {
        
        var park: TaxiPark
        switch taxiParkSegmentedControl.selectedSegmentIndex {
        case 1:
            park = MaximPark()
        case 2:
            park = SelfPark()
        default:
            park = YandexPark()
        }
        dataService.getOrderDetails(forCost: Double(costSliderControl.value), distance: Double(distanceSliderControl.value), park: park)
        
        showPullUpViewButtonPressed(self)
        updateFields()
    }
    
    @IBAction private func showPullUpViewButtonPressed(_ sender: Any) {
        
        if pullUpViewHeightConstraint.constant == 0 {
            animatePullUpView(withText: "Скрыть", andHeightConstraintValue: 300)
        } else {
            animatePullUpView(withText: "Добавить", andHeightConstraintValue: 0)
        }
    }
    
    // MARK: Slider actions
    
    @IBAction private func costSliderControlValueChanged(_ sender: Any) {
        let currentValue = Int(costSliderControl.value)
        costLabel.text = String(currentValue)
    }
    
    @IBAction private func distanceSliderControlValueChanged(_ sender: Any) {
        let currentValue = Int(distanceSliderControl.value)
        distanceLabel.text = String(currentValue)
    }
}


extension MainVC {
    
    // MARK: Gesture Recognizers
    
    private func setupGestureRecogizers() {
        
        let swipeToClean = UISwipeGestureRecognizer(target: self, action: #selector(clean))
        swipeToClean.direction = .right
        informationContainerView.addGestureRecognizer(swipeToClean)
        
        let swipeToIncreaseCostValue = UISwipeGestureRecognizer(target: self, action: #selector(increaseCostValue))
        swipeToIncreaseCostValue.direction = .right
        costContainerView.addGestureRecognizer(swipeToIncreaseCostValue)
        
        let swipeToDecreaseCostValue = UISwipeGestureRecognizer(target: self, action: #selector(decreaseCostValue))
        swipeToDecreaseCostValue.direction = .left
        costContainerView.addGestureRecognizer(swipeToDecreaseCostValue)
        
        let swipeToIncreaseDistanceValue = UISwipeGestureRecognizer(target: self, action: #selector(increaseDistanceValue))
        swipeToIncreaseDistanceValue.direction = .right
        distanceContainerView.addGestureRecognizer(swipeToIncreaseDistanceValue)
        
        let swipeToDecreaseDistanceValue = UISwipeGestureRecognizer(target: self, action: #selector(decreaseDistanceValue))
        swipeToDecreaseDistanceValue.direction = .left
        distanceContainerView.addGestureRecognizer(swipeToDecreaseDistanceValue)
    }
    
    @objc private func clean() {
        dataService.clean()
        updateFields()
    }
    
    @objc private func increaseCostValue() {
        costSliderControl.value += 1
        let currentValue = Int(costSliderControl.value)
        costLabel.text = String(currentValue)
    }
    
    @objc private func decreaseCostValue() {
        costSliderControl.value -= 1
        let currentValue = Int(costSliderControl.value)
        costLabel.text = String(currentValue)
    }
    
    @objc private func increaseDistanceValue() {
        distanceSliderControl.value += 1
        let currentValue = Int(distanceSliderControl.value)
        distanceLabel.text = String(currentValue)
    }
    
    @objc private func decreaseDistanceValue() {
        distanceSliderControl.value -= 1
        let currentValue = Int(distanceSliderControl.value)
        distanceLabel.text = String(currentValue)
    }
}
