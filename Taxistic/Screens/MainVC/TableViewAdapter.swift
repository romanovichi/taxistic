//
//  TableViewAdapter.swift
//  Taxistic
//
//  Created by Иван Романович on 24.09.2018.
//  Copyright © 2018 Иван Романович. All rights reserved.
//

import Foundation
import UIKit

class TableViewAdapter: NSObject {
    
    let table: UITableView
    var orders: [Order]
    
    init(tableView: UITableView, orders: [Order]) {
        table = tableView
        self.orders = orders
        
        tableView.register(UINib.init(nibName: "OrderCell", bundle: nil), forCellReuseIdentifier: "OrderCell")
        
        tableView.tableFooterView = UIView()
        tableView.allowsSelection = false
        
        super.init()
        
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    func updateTable(with orders: [Order]) {
        self.orders = orders
        table.reloadData()
    }
}

extension TableViewAdapter: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return orders.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "OrderCell", for: indexPath) as? OrderCell {
            cell.set(order: orders[indexPath.row])
            return cell
        }
        return UITableViewCell()
    }
}
