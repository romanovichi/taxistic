//
//  SettingsVC.swift
//  Taxistic
//
//  Created by Иван Романович on 07.01.2019.
//  Copyright © 2019 Иван Романович. All rights reserved.
//

import UIKit
import SwiftyUserDefaults

class SettingsVC: BaseVC {
    
    @IBOutlet weak var subsidySwitchController: UISwitch!
    @IBOutlet weak var fuelTextField: UITextField!
    @IBOutlet weak var goalTextField: UITextField!
    
    var onCancel: (() -> ())?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        fuelTextField.text = String(Defaults[.fuel])
        goalTextField.text = String(Int(Defaults[.goal]))
        
        subsidySwitchController.onTintColor = #colorLiteral(red: 1, green: 0.7970581889, blue: 0.1291670807, alpha: 1)
        goalTextField.backgroundColor = #colorLiteral(red: 0.3321701288, green: 0.3321786821, blue: 0.3321741223, alpha: 1)
        goalTextField.textColor = #colorLiteral(red: 1, green: 0.7970581889, blue: 0.1291670807, alpha: 1)
        fuelTextField.backgroundColor = #colorLiteral(red: 0.3321701288, green: 0.3321786821, blue: 0.3321741223, alpha: 1)
        fuelTextField.textColor = #colorLiteral(red: 1, green: 0.7970581889, blue: 0.1291670807, alpha: 1)
        
        if Defaults[.subsidyIsOn] {
            subsidySwitchController.setOn(true, animated: false)
        } else {
            subsidySwitchController.setOn(false, animated: false)
        }

        navigationBar.addNavigationButton(.backButton) {
            self.back()
        }
    }
    
    private func back() {
        self.navigationController?.popViewController(animated: true)
    }

    @IBAction private func cancelLastOrder(_ sender: Any) {
        onCancel!()
    }
    
    @IBAction private func saveSettings(_ sender: Any) {
        if subsidySwitchController.isOn {
            Defaults[.subsidyIsOn] = true
        } else {
            Defaults[.subsidyIsOn] = false
        }
        if let value = Double(fuelTextField.text!) {
            Defaults[.fuel] = value
        }
        if let value = Double(goalTextField.text!) {
            Defaults[.goal] = value
        }
    }
}
