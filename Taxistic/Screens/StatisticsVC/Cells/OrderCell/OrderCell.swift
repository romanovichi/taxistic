//
//  OrderCell.swift
//  Taxistic
//
//  Created by Иван Романович on 24.09.2018.
//  Copyright © 2018 Иван Романович. All rights reserved.
//

import UIKit

class OrderCell: UITableViewCell {

    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var ordersLabel: UILabel!
    @IBOutlet weak var profitLabel: UILabel!
    @IBOutlet weak var subsidyLabel: UILabel!
    @IBOutlet weak var costLabel: UILabel!
    
    private var time: Date! {
        didSet {
            timeLabel.text = formateDate(time)
        }
    }
    
    private var cost: Double! {
        didSet {
            costLabel.text = String(Int(cost))
            costLabel.sizeToFit()
        }
    }
    
    private var distance: Double! {
        didSet {
            ordersLabel.text = String(Int(distance))
        }
    }
    
    private var amount: Double! {
        didSet {
            profitLabel.text = String(Int(amount))
        }
    }
    
    private var subsidy: String! {
        didSet {
            subsidyLabel.text = subsidy
        }
    }
    
    func set(order: Order) {
        self.cost = order.cost
        self.time = order.endTime
        self.distance = order.distance
        self.amount = order.profit
        self.subsidy = order.subsidyDescription
    }
    
    private func formateDate(_ date: Date) -> String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.timeStyle = .short
        let dateString = dateFormatter.string(from: date)
        
        return dateString
    }
    
}
