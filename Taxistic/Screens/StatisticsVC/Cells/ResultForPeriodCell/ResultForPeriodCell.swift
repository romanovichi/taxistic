//
//  HeaderView.swift
//  Taxistic
//
//  Created by Иван Романович on 02.10.2018.
//  Copyright © 2018 Иван Романович. All rights reserved.
//

import UIKit

class ResultForPeriodCell: UITableViewCell {
    
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var totalOrdersLabel: UILabel!
    @IBOutlet weak var totalProfitLabel: UILabel!
    @IBOutlet weak var totalDistanceLabel: UILabel!
    @IBOutlet weak var totalHoursLabel: UILabel!
    @IBOutlet weak var totalFuelLabel: UILabel!
    
    private var date: Date! {
        didSet {
            dateLabel.text = formateDate(date)
        }
    }
    
    private var orders: Double! {
        didSet {
            totalOrdersLabel.text = String(Int(orders))
        }
    }
    
    private var amount: Double! {
        didSet {
            totalProfitLabel.text = String(Int(amount)) + " ₽"
        }
    }
    
    private var distance: Double! {
        didSet {
            totalDistanceLabel.text = String(Int(distance))
        }
    }
    
    private var fuel: Double! {
        didSet {
            totalFuelLabel.text = String(Int(fuel))
        }
    }
    
    private var avarageProfit: Double! {
        didSet {
            totalHoursLabel.text = String(Int(avarageProfit))
        }
    }
    
    func set(resultForPeriod: ResultForPeriod) {
        self.date = resultForPeriod.date
        self.orders = resultForPeriod.ordersTotal
        self.amount = resultForPeriod.profitTotal
        self.distance = resultForPeriod.distanceTotal
        self.fuel = resultForPeriod.fuelTotal
        self.avarageProfit = resultForPeriod.avarageProfit
    }
    
    private func formateDate(_ date: Date) -> String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "ru_RU")
        dateFormatter.dateFormat = "EEEE, dd MMMM, yyyy"
        let dateString = dateFormatter.string(from: date)
        
        return dateString.capitalized
    }
}
