//
//  StatisticsVC.swift
//  Taxistic
//
//  Created by Иван Романович on 28.09.2018.
//  Copyright © 2018 Иван Романович. All rights reserved.
//

import UIKit

class StatisticsVC: BaseVC {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var monthProfitLabel: UILabel!
    @IBOutlet weak var weekProfitLabel: UILabel!
    @IBOutlet weak var statisticScaleSegmentedControl: UISegmentedControl!
    
    private var groupedOrders: [(key: Date, value: [Order])]?
    private var orders: [Order]?
    private var sectionIsExpanded = [Bool]()
    private var calculaionService = OrdersCalculationService()
    private var sortingService = OrdersSortingService()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.register(UINib.init(nibName: "OrderCell", bundle: nil), forCellReuseIdentifier: "OrderCell")
        tableView.register(UINib.init(nibName: "ResultForPeriodCell", bundle: nil), forCellReuseIdentifier: "ResultForPeriodCell")
        
        tableView.tableFooterView = UIView()
        tableView.allowsSelection = true
        
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 70
        
        tableView.delegate = self
        tableView.dataSource = self
        
        if let orders = orders {
            monthProfitLabel.text = String(Int(calculaionService.getTotalProfit(for: sortingService.getOrdersForCurrentMonth(from: orders))))
            weekProfitLabel.text = String(Int(calculaionService.getTotalProfit(for: sortingService.getOrdersForCurrentWeek(from: orders))))
        }
    
        navigationBar.addNavigationButton(.backButton) { 
            self.back()
        }
    }
    
    convenience init(orders: [Order]) {
        self.init()
        self.orders = orders
        self.groupedOrders = sortingService.group(orders: orders, by: .day)
        
        if let ordersList = groupedOrders {
            for _ in 0...ordersList.count {
                sectionIsExpanded.append(false)
            }
        }
    }
    
    @IBAction private func statisticScaleSegmentedControlChanged(_ sender: Any) {
        if let segmentedControl = sender as? UISegmentedControl {
            guard let orders = orders else { return }
            switch segmentedControl.selectedSegmentIndex {
            case 1:
                groupedOrders = sortingService.group(orders: orders, by: .week)
            case 2:
                groupedOrders = sortingService.group(orders: orders, by: .month)
            default:
                groupedOrders = sortingService.group(orders: orders, by: .day)
            }
        }
        tableView.reloadData()
    }
    
    func back() {
        self.navigationController?.popViewController(animated: true)
    }
}

extension StatisticsVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if let groupedOrders = groupedOrders {
            return groupedOrders.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if sectionIsExpanded[section] {
            if let groupedOrders = groupedOrders {
                return groupedOrders[section].value.count + 1
            }
        } else {
            return 1
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let groupedOrders = groupedOrders else { return UITableViewCell() }
            if indexPath.row == 0 {
                if let cell = tableView.dequeueReusableCell(withIdentifier: "ResultForPeriodCell", for: indexPath) as? ResultForPeriodCell {
                    cell.set(resultForPeriod: calculaionService.getTotalStatistic(for: groupedOrders[indexPath.section].value))
                    return cell
                }
            } else{
                if let cell = tableView.dequeueReusableCell(withIdentifier: "OrderCell", for: indexPath) as? OrderCell {
                    cell.set(order: groupedOrders[indexPath.section].value[indexPath.row - 1])
                    return cell
                }
            }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if sectionIsExpanded[indexPath.section] {
            sectionIsExpanded[indexPath.section] = false
            let sections = IndexSet.init(integer: indexPath.section)
            tableView.reloadSections(sections, with: .none)
        } else {
            sectionIsExpanded[indexPath.section] = true
            let sections = IndexSet.init(integer: indexPath.section)
            tableView.reloadSections(sections, with: .none)
        }
    }
}
