//
//  CalculationService.swift
//  Taxistic
//
//  Created by Иван Романович on 14.11.2018.
//  Copyright © 2018 Иван Романович. All rights reserved.
//

import Foundation
import SwiftyUserDefaults

class OrdersCalculationService {
    
    func getTotalProfit(for orders: [Order]) -> Double {
        var profit = 0.0
        for order in orders {
            profit += order.profit
        }
        return profit
    }
    
    func getTotalStatistic(for orders: [Order]) -> ResultForPeriod {
        
        let result = ResultForPeriod()
        result.date = orders[0].endTime
        orders.forEach({ (order) in
            result.ordersTotal += 1
            result.profitTotal += order.profit
            result.distanceTotal += order.distance
            result.fuelTotal += order.fuelСonsumption
        })
        result.avarageProfit = result.profitTotal / result.ordersTotal
        return result
    }
    
    func getTotalFuelСonsumption(for orders: [Order]) -> Double {
        var fuelСonsumption = 0.0
        for order in orders {
            fuelСonsumption += order.fuelСonsumption
        }
        return fuelСonsumption
    }
    
    func getAvagageProfit(for orders: [Order]) -> Double {
        
        if !orders.isEmpty {
            var totalProfit: Double = 0.0
            orders.forEach { (order) in
                totalProfit += order.profit
            }
            let avarageProfit = totalProfit / Double(orders.count)
            return avarageProfit
        }
        return 0.0
    }
    
    func getOrdersAmountToReachDailyGoal(_ orders: [Order], _ avarageProfit: Double) -> Double {
        var result = 0.0
        if !orders.isEmpty {
            result = (Defaults[.goal] - getTotalProfit(for: orders)) / avarageProfit
            let roundedResult = result.rounded(.up)
            return roundedResult
        }
        return result
    }
    
    func getProgress(for orders: [Order]) -> Float {
        let result = Float(getTotalProfit(for: orders) / Defaults[.goal])
        return result
    }
}
