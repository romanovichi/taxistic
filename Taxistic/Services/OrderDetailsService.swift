//
//  CalculationService.swift
//  Taxistic
//
//  Created by Иван Романович on 15.09.2018.
//  Copyright © 2018 Иван Романович. All rights reserved.
//

import Foundation
import SwiftyUserDefaults

class OrderDetailsService {
    
    var taxiPark: TaxiPark
    
    init(_ taxiPark: TaxiPark) {
        self.taxiPark = taxiPark
    }
}

extension OrderDetailsService {
    
    func getOrderDetails(from cost: Double, distance: Double, and endTime: Date) -> Order {
        
        let (income, description) = taxiPark.getNetProfit(for: cost)
        let profit = income - getFuelСonsumption(for: distance, and: Defaults[.fuel])
        let fuelСonsumption = getFuelСonsumption(for: distance, and: Defaults[.fuel])
        
        let order = Order(cost: cost, profit: profit, subsidyDescription: description, distance: distance, fuelСonsumption: fuelСonsumption, endTime: endTime)
        print(order.profit, order.subsidyDescription)
        
        return order
    }
    
    private func getFuelСonsumption(for distance: Double, and сonsumption: Double) -> Double {
        return distance * сonsumption / 100 * 40
    }
}
