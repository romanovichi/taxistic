//
//  DataService.swift
//  Taxistic
//
//  Created by Иван Романович on 14.09.2018.
//  Copyright © 2018 Иван Романович. All rights reserved.
//

import Foundation

class OrdersDataService {
    
    private let repository: Repository<MainData>
    private var mainData = MainData()
    
    init() {
        repository = Repository<MainData>()
    }
}

extension OrdersDataService {
    
    func getOrderDetails(forCost cost: Double, distance: Double, park: TaxiPark) {
        
        mainData = get()
        let orderDetailsService = OrderDetailsService(park)
        let order = orderDetailsService.getOrderDetails(from: cost, distance: distance, and: Date())
        
        repository.update {
            self.mainData.orders.append(order)
        }
        
        repository.updateById(mainData)
    }
}

extension OrdersDataService {
    
    func getAllOrders() -> [Order] {
        return Array(get().orders)
    }
}

extension OrdersDataService {
    
    func get() -> MainData {
        
        var mainData = repository.get()
        
        if mainData.isEmpty {
            mainData.append(MainData(withStartTime: Date()))
            repository.updateById(mainData.first!)
            return mainData.first!
        } else {
            return mainData.first! 
        }
    }
    
    func removeLastOrder() {
        mainData = get()
        repository.update {
            self.mainData.orders.removeLast()
        }
    }
    
    func clean() {
        mainData = get()
        repository.cleanMainData(mainData)
        repository.update {
            self.mainData.startTime = Date()
        }
    }
}
