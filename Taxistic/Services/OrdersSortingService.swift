//
//  SortingService.swift
//  Taxistic
//
//  Created by Иван Романович on 14.01.2019.
//  Copyright © 2019 Иван Романович. All rights reserved.
//

import Foundation

enum SortingScale {
    case day
    case week
    case month
}

class OrdersSortingService {
    
    func group(orders: [Order], by scale: SortingScale) -> [(key: Date, value: [Order])] {
        
        let groupedOrders = Dictionary(grouping: orders) { (element) -> Date in
            
            var year: Int?
            var month: Int?
            var weekOfYear: Int?
            var day: Int?
            var components = DateComponents()
            
                switch scale {
                case .week:
                    year = element.endTime.year
                    weekOfYear = element.endTime.week
                    components = DateComponents(weekOfYear: weekOfYear, yearForWeekOfYear: year)
                case .month:
                    year = element.endTime.year
                    month = element.endTime.month
                    components = DateComponents(year: year, month: month)
                default:
                    year = element.endTime.year
                    month = element.endTime.month
                    day = element.endTime.day
                    components = DateComponents(year: year, month: month, day: day)
                }
            
            let date = Calendar.current.date(from: components)
            
            return date!
        }
        let sortedKeysAndValues = groupedOrders.sorted { $0.key > $1.key }
        return sortedKeysAndValues
    }
    
    func getOrdersFor(date: Date, from orders: [Order]) -> [Order] {
        
        let dayStartTime = Calendar.current.date(bySettingHour: 4, minute: 0, second: 0, of: Date())
        let dayEndTime = Calendar.current.date(byAdding: .day, value: 1, to: dayStartTime!)
        
        let range = dayStartTime!...dayEndTime!
        let filtredOrders = orders.filter({ range.contains($0.endTime) })
        
        return Array(filtredOrders)
    }
    
    func getOrdersForCurrentMonth(from orders: [Order]) -> [Order] {
        
        let filtredOrders = orders.filter({ ($0.endTime).isDateinThisMounth })
        
        return Array(filtredOrders)
    }
    
    func getOrdersForCurrentWeek(from orders: [Order]) -> [Order] {
        
        let startOfWeek = Date().startOfWeek
        let endOfWeek = Date().endOfWeek
        let range = startOfWeek!...endOfWeek!
        var filtredOrders: [Order] = []
        
        orders.forEach { (order) in
            if range.contains(order.endTime) {
                filtredOrders.append(order)
            }
        }
        return filtredOrders
    }
}

