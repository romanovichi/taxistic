//
//  MaximPark.swift
//  Taxistic
//
//  Created by Иван Романович on 18.09.2018.
//  Copyright © 2018 Иван Романович. All rights reserved.
//

import Foundation

class MaximPark: TaxiPark {
    
    func getNetProfit(for cost: Double) -> (Double, String) {
        return (cost * 0.90, "нет")
    }
}
