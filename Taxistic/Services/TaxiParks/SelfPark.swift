//
//  Self.swift
//  Taxistic
//
//  Created by Иван Романович on 10.01.2019.
//  Copyright © 2019 Иван Романович. All rights reserved.
//

import Foundation

class SelfPark: TaxiPark {
    
    func getNetProfit(for cost: Double) -> (Double, String) {
        return (cost, "нет")
    }
}
