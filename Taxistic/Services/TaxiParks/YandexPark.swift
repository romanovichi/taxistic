//
//  YandexPark.swift
//  Taxistic
//
//  Created by Иван Романович on 17.09.2018.
//  Copyright © 2018 Иван Романович. All rights reserved.
//

import Foundation
import SwiftyUserDefaults

class YandexPark: TaxiPark {
    
    func getSubsidy(for cost: Double) -> (Double, String) {
        
        var subsidy: Double = 0
        var subsidyAmountString: String = ""
        
        let currentDate = Date()
        
        let currentDay = Calendar.current.component(.weekday, from: currentDate)
        let currentHour = Calendar.current.component(.hour, from: currentDate)
        
        if currentDay == 2 || currentDay == 3 || currentDay == 4 || currentDay == 5 {
            if currentHour >= 7 && currentHour < 9 || currentHour >= 17 && currentHour < 19 {
                if cost < 150 {
                    subsidyAmountString = "до 150"
                    subsidy = 150 - cost
                }
            }
            if currentHour >= 9 && currentHour < 17 {
                if cost < 110 {
                    subsidyAmountString = "до 110"
                    subsidy = 110 - cost
                }
            }
            if currentHour >= 19 && currentHour <= 23 {
                if cost < 120 {
                    subsidyAmountString = "до 120"
                    subsidy = 120 - cost
                }
            }
            if currentHour >= 0 && currentHour < 7 {
                if cost < 100 {
                    subsidyAmountString = "до 100"
                    subsidy = 100 - cost
                }
            }
        }
        
        if currentDay == 6 {
            if currentHour >= 7 && currentHour < 9 || currentHour >= 17 && currentHour < 19 || currentHour >= 22 && currentHour <= 23{
                if cost < 150 {
                    subsidyAmountString = "до 150"
                    subsidy = 150 - cost
                }
            }
            if currentHour >= 9 && currentHour < 17 {
                if cost < 110 {
                    subsidyAmountString = "до 110"
                    subsidy = 110 - cost
                }
            }
            if currentHour >= 19 && currentHour < 22 {
                if cost < 120 {
                    subsidyAmountString = "до 120"
                    subsidy = 120 - cost
                }
            }
            if currentHour >= 0 && currentHour < 7 {
                if cost < 100 {
                    subsidyAmountString = "до 100"
                    subsidy = 100 - cost
                }
            }
        }
        
        if currentDay == 7 || currentDay == 1 {
            if currentHour >= 0 && currentHour < 2 || currentHour >= 22 && currentHour <= 23 {
                if cost < 150 {
                    subsidyAmountString = "до 150"
                    subsidy = 150 - cost
                }
            }
            if currentHour >= 7 && currentHour < 22 {
                if cost < 110 {
                    subsidyAmountString = "до 120"
                    subsidy = 110 - cost
                }
            }
            if currentHour >= 2 && currentHour < 7 {
                if cost < 100 {
                    subsidyAmountString = "до 100"
                    subsidy = 100 - cost
                }
            }
        }
        return (subsidy, subsidyAmountString)
    }
    
    func getNetProfit(for cost: Double) -> (Double, String) {
        var (subsidy, subsidyAmountString) = (0.0, "нет")
        if Defaults[.subsidyIsOn] {
            (subsidy, subsidyAmountString) = getSubsidy(for: cost)
        }
        return (cost * 0.784 + (subsidy * 0.85) * 0.94, subsidyAmountString)
    }
}
